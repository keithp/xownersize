/*
 * Copyright © 2018 Keith Packard
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that copyright
 * notice and this permission notice appear in supporting documentation, and
 * that the name of the copyright holders not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  The copyright holders make no representations
 * about the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
/*
 * Copyright (c) 1999, 2010, Oracle and/or its affiliates. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/*

Copyright 1987, 1998  The Open Group

Permission to use, copy, modify, distribute, and sell this software and its
documentation for any purpose is hereby granted without fee, provided that
the above copyright notice appear in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation.

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL
INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING
FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Except as contained in this notice, the name of a copyright holder
shall not be used in advertising or otherwise to promote the sale, use
or other dealings in this Software without prior written authorization
of the copyright holder.

*/


/*
 * xownersize.c	- Set or display X window owner size information
 *
 *  Author:	Keith Packard
 *		2018-8-26
 *
 * Derived from xwininfo, which is the MIT Project Athena, X Window
 * system window information utility.
 *
 *  Author:	Mark Lillibridge, MIT Project Athena
 *		16-Jun-87
 */

#include "config.h"

#include <xcb/xcb.h>
#include <xcb/xproto.h>
#include <xcb/composite.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <math.h>

/* Include routines to handle parsing defaults */
#include "dsimple.h"

static xcb_connection_t *dpy;
static xcb_screen_t *screen;

/*
 * Report the syntax for calling xownersize:
 */
_X_NORETURN _X_COLD
static void
usage (void)
{
    fprintf (stderr,
	     "usage:  %s [-options ...]\n\n"
	     "where options include:\n"
	     "    -help                print this message\n"
	     "    -version             print version message\n"
	     "    -display host:dpy    X server to contact\n"
	     "    -id windowid         use the window with the specified id\n"
	     "    -name windowname     use the window with the specified name\n"
	     "    -frame               don't ignore window manager frames\n"
	     "	  -geometry <w>x<h>    set owner size to specific value\n"
	     "    -scale <xs>x<ys>     scale window by specified amounts\n"
	     "\n",
	     program_name);
    exit (1);
}

/* end of pixel to inch, metric converter */

int
main (int argc, char **argv)
{
    register int i;
    int owner_width = 0, owner_height = 0;
    int set_size = 0;
    int set_scale = 0;
    int show_size = 0;
    int frame = 0;
    double owner_scale_x = 0, owner_scale_y = 0;
    xcb_window_t window = 0;
    char *display_name = NULL;
    const char *window_name = NULL;
    xcb_get_geometry_reply_t *geometry;
    xcb_composite_get_owner_window_size_reply_t *owner_size;

    program_name = argv[0];

    /* Handle our command line arguments */
    for (i = 1; i < argc; i++) {
	if (!strcmp (argv[i], "-help"))
	    usage ();
	if (!strcmp (argv[i], "-display") || !strcmp (argv[i], "-d")) {
	    if (++i >= argc)
		Fatal_Error("-display requires argument");
	    display_name = argv[i];
	    continue;
	}
	if (!strcmp (argv[i], "-id")) {
	    if (++i >= argc)
		Fatal_Error("-id requires argument");
	    window = strtoul(argv[i], NULL, 0);
	    continue;
	}
	if (!strcmp (argv[i], "-name")) {
	    if (++i >= argc)
		Fatal_Error("-name requires argument");
	    window_name = argv[i];
	    continue;
	}
	if (!strcmp (argv[i], "-frame")) {
	    frame = 1;
	    continue;
	}
	if (!strcmp (argv[i], "-geometry")) {
	    if (set_scale)
		Fatal_Error("specify only one of -geometry or -scale");
	    if (++i >= argc)
		Fatal_Error("-geometry requires argument");
	    if (sscanf(argv[i], "%dx%d", &owner_width, &owner_height) != 2)
		Fatal_Error("-geometry requres <width>x<height>\n");
	    set_size = 1;
	    continue;
	}
	if (!strcmp (argv[i], "-scale")) {
	    if (set_size)
		Fatal_Error("specify only one of -geometry or -scale");
	    if (++i >= argc)
		Fatal_Error("-scale requires argument");
	    if (sscanf(argv[i], "%lfx%lf", &owner_scale_x, &owner_scale_y) != 2)
		Fatal_Error("-scale requres <scale-x>x<scale-y>\n");
	    if (owner_scale_x == 0 || owner_scale_y == 0)
		Fatal_Error("-scale requres non-zero scale factors");
	    set_scale = 1;
	    continue;
	}
	if (!strcmp(argv[i], "-version")) {
	    puts(PACKAGE_STRING);
	    exit(0);
	}
	fprintf (stderr, "%s: unrecognized argument %s\n\n",
		 program_name, argv[i]);
	usage ();
    }

    Setup_Display_And_Screen (display_name, &dpy, &screen);

    if (window_name) {
	window = Window_With_Name (dpy, screen->root, window_name);
	if (!window)
	    Fatal_Error ("No window with name \"%s\" exists!", window_name);
    }

    /* If no window selected on command line, let user pick one the hard way */
    if (!window) {
	printf ("\n"
		"xownersize: Please select the window which you\n"
		"            would like to manage by clicking the\n"
		"            mouse in that window.\n");
	Intern_Atom (dpy, "_NET_VIRTUAL_ROOTS");
	Intern_Atom (dpy, "WM_STATE");
	window = Select_Window (dpy, screen, !frame);
    }

    /*
     * Do the actual displaying as per parameters
     */
    if (!(set_size || set_scale))
	show_size = 1;

    /*
     * make sure that the window is valid
     */
    {
	xcb_generic_error_t	*gg_err, *go_err;

	xcb_get_geometry_cookie_t gg_cookie =
	    xcb_get_geometry (dpy, window);
	xcb_composite_get_owner_window_size_cookie_t go_cookie =
	    xcb_composite_get_owner_window_size (dpy, window);

	geometry = xcb_get_geometry_reply(dpy, gg_cookie, &gg_err);

	owner_size = xcb_composite_get_owner_window_size_reply(dpy, go_cookie, &go_err);

	if (!geometry) {
	    if (gg_err)
		Print_X_Error (dpy, gg_err);

	    Fatal_Error ("No such window with id 0x%x.", window);
	}

	if (!owner_size) {
	    if (go_err)
		Print_X_Error (dpy, go_err);

	    Fatal_Error ("Cannot get owner window size of id 0x%x.", window);
	}
    }

    xcb_flush (dpy);

    if (show_size) {
	printf ("Window: 0x%x current size %dx%d owner size %dx%d scale factor %fx%f\n",
		window,
		geometry->width, geometry->height,
		owner_size->width, owner_size->height,
		owner_size->width ? (double) geometry->width / (double) owner_size->width : 0.0,
		owner_size->height ? (double) geometry->height / (double) owner_size->height : 0.0);
    } else {
	if (set_scale) {
	    owner_width = floor (geometry->width / owner_scale_x + 0.5);
	    owner_height = floor (geometry->height / owner_scale_y + 0.5);
	}
	xcb_composite_set_owner_window_size(dpy, window, owner_width, owner_height);
	xcb_flush(dpy);
    }

    xcb_disconnect (dpy);
    exit (0);
}
