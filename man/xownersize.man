.\"
.\" Copyright © 2018 Keith Packard
.\"
.\" Permission to use, copy, modify, distribute, and sell this software and its
.\" documentation for any purpose is hereby granted without fee, provided that
.\" the above copyright notice appear in all copies and that both that copyright
.\" notice and this permission notice appear in supporting documentation, and
.\" that the name of the copyright holders not be used in advertising or
.\" publicity pertaining to distribution of the software without specific,
.\" written prior permission.  The copyright holders make no representations
.\" about the suitability of this software for any purpose.  It is provided "as
.\" is" without express or implied warranty.
.\"
.\" THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
.\" INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
.\" EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT OR
.\" CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
.\" DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
.\" TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
.\" OF THIS SOFTWARE.
.\"
.\" Copyright 1988, 1998  The Open Group
.\"
.\" Permission to use, copy, modify, distribute, and sell this software and its
.\" documentation for any purpose is hereby granted without fee, provided that
.\" the above copyright notice appear in all copies and that both that
.\" copyright notice and this permission notice appear in supporting
.\" documentation.
.\"
.\" The above copyright notice and this permission notice shall be included
.\" in all copies or substantial portions of the Software.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
.\" OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
.\" IN NO EVENT SHALL THE OPEN GROUP BE LIABLE FOR ANY CLAIM, DAMAGES OR
.\" OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
.\" ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
.\" OTHER DEALINGS IN THE SOFTWARE.
.\"
.\" Except as contained in this notice, the name of The Open Group shall
.\" not be used in advertising or otherwise to promote the sale, use or
.\" other dealings in this Software without prior written authorization
.\" from The Open Group.
.\"
.\"
.TH XOWNERSIZE 1 __xorgversion__
.SH NAME
xownersize \- set or display window owner size information
.SH SYNOPSIS
.B "xownersize"
[\-help] [\-id \fIid\fP] [\-name \fIname\fP]
[\-frame] [-geometry <w>x<h>] [-scale <xs>x<ys>]
[\-display \fIdisplay\fP] [\-version]
.SH DESCRIPTION
.PP
.I Xownersize
is a utility for setting or displaying owner size information for a window.
.PP
The user has the option of selecting the target window with
the mouse (by clicking any mouse button in the desired window) or by
specifying its window id on the command line with the \fB\-id\fP option.
Or instead of specifying
the window by its id number, the \fB\-name\fP option may be used to specify
which window is desired by name.
.SH OPTIONS
.PP
.TP 8
.B "\-help"
Print out the `Usage:' command syntax summary.
.PP
.TP 8
.B "\-id \fIid\fP"
This option allows the user to specify a target window \fIid\fP on the
command line rather than using the mouse to select the target window.
This is very useful in debugging X applications where the target
window is not mapped to the screen or where the use of the mouse might
be impossible or interfere with the application.
.PP
.TP 8
.B "\-name \fIname\fP"
This option allows the user to specify that the window named \fIname\fP
is the target window on the command line rather than using the mouse to
select the target window.
.PP
.TP 8
.B \-frame
This option causes window manager frames to be considered when manually
selecting windows.
.PP
.TP 8
.B "\-geometry <w>x<h>"
This option allows the user to set the target window owner size to the
specified width and height values.
.PP
.TP 8
.B "\-scale <xs>x<ys>"
This option allows the user to set the target window ownersize to the
current size divided by the specified x scale and y scale factors.
.PP
.TP 8
.B \-display \fIdisplay\fP
This option allows you to specify the server to connect to; see \fIX(__miscmansuffix__)\fP.
.PP
.TP 8
.B \-version
This option indicates that
.I xownersize
should print its version information and exit.
.SH EXAMPLE
.PP
The following is a sample summary taken with no options specified:

Window: 0x40000d current size 1834x1187 owner size 917x594 scale factor 2.000000x1.998316

.SH ENVIRONMENT
.PP
.TP 8
.B DISPLAY
To get the default host and display number.
.SH SEE ALSO
.IR X (__miscmansuffix__),
.SH AUTHOR
Keith Packard
